/*
 * PROJETO IFRS - CPW2
 * AUTHOR: FÁBIO B. TRAMASOLI - 0619132
 * SOKOBAN
 * LEVELS FROM: http://www.sourcecode.se/sokoban/levels?sort=5
 */
// CONSTANTES
var EnumObjectsTypes = {
  BOX: "box",
  OBJECTIVE: "objective",
  BOX_OBJECTIVE: "box-objective",
  OBSTACLE: "obstacle",
  EMPTY: "empty",
  CHARACTER: "character",
  CHARACTER_OBJECTIVE: "character-objective"
};

var EnumObjectsBehavior = {
  DYNAMIC: 0,
  STATIC: 1
};

var KEYS = {
    left: 37,
    up: 38,
    right: 39,
    down: 40
};

// VARIÁVEIS
var TABULEIRO_STATIC;
var TABULEIRO_SIZE;
var MOVIMENTOS;
var CHAR_POS;
var GAME_TABLE;
var REMAINING_OBJECTIVES=0;
var xmlhttpGameConfig = new XMLHttpRequest();

// FUNÇÕES PRINCIPAIS
// Está bem, código 99% autoral...
// retirado de http://stackoverflow.com/questions/2532218/pick-random-property-from-a-javascript-object
function pickRandomProperty(obj) {
    var result;
    var count = 0;
    for (var prop in obj)
        if (Math.random() < 1/++count)
           result = prop;
    return result;
}

function pickRandomElement(type) {
    var randElement="";
    while(!testObjectBehavior(randElement,type))
        randElement=EnumObjectsTypes[pickRandomProperty(EnumObjectsTypes)];
    return randElement;
}

function testObjectBehavior(object, behavior) {
    return (
            object.toString().toUpperCase() in EnumObjectsTypes
            &&
            object !== EnumObjectsTypes.OBJECTIVE
            &&
            (
                ([EnumObjectsTypes.BOX].indexOf(object)!==-1 && behavior === EnumObjectsBehavior.DYNAMIC)
                ||
                ([EnumObjectsTypes.BOX, EnumObjectsTypes.CHARACTER].indexOf(object)===-1 && behavior === EnumObjectsBehavior.STATIC)
            ) 
           );
}

function gameInitialValues() {
    if (xmlhttpGameConfig.readyState === 4 && xmlhttpGameConfig.status === 200){
        CONFIG=JSON.parse(xmlhttpGameConfig.responseText);
        TABULEIRO_STATIC=CONFIG.TABULEIRO;
        //TABULEIRO_STATIC=[["","","obstacle","obstacle","obstacle","obstacle"],["","obstacle","empty","empty","empty","obstacle"],["obstacle","character","box","empty","box","obstacle"],["obstacle","empty","empty","objective","objective","obstacle"],["obstacle","empty","obstacle","obstacle","obstacle",""],["obstacle","obstacle","","","",""]];
        TABULEIRO_SIZE= {
            x: TABULEIRO_STATIC[0].length,
            y: TABULEIRO_STATIC.length
        };

        // VARIÁVEIS
        MOVIMENTOS=0;
        CHAR_POS = {
            posX: Math.round(TABULEIRO_SIZE.x/2),
            posY: Math.round(TABULEIRO_SIZE.y/2)
        };
        document.getElementById("game-status").innerHTML="[STATUS]";
        REMAINING_OBJECTIVES=0;
        GAME_TABLE = createGameTableElement("");
    }
    
}

function createGameTableElement(className) {
    GAME_TABLE = document.createElement("TABLE");
    GAME_TABLE.id="game-table";
    GAME_TABLE.border=1;
    //Não suportado no HTML5... vai saber.
    //http://www.w3schools.com/tags/att_table_cellspacing.asp
    //GAME_TABLE.cellSpacing=0;
    GAME_TABLE.className=(isEmpty(className))?"game-table":className;
    for(var y=0; y<TABULEIRO_SIZE.y; y++) {
        var row = GAME_TABLE.insertRow(y);
        for(var x=0; x<TABULEIRO_SIZE.x; x++) {
            var cell = row.insertCell(x);
            cell.className=TABULEIRO_STATIC[y][x];
            cell.innerHTML="&nbsp;";
            if(cell.className===EnumObjectsTypes.CHARACTER) {
                CHAR_POS.posY=y;
                CHAR_POS.posX=x;
            }
            else if(cell.className===EnumObjectsTypes.OBJECTIVE) {
                REMAINING_OBJECTIVES++;
            }
        }
    };
    return GAME_TABLE;
}

function initializeGame(containerElement) {
    xmlhttpGameConfig = new XMLHttpRequest();
    xmlhttpGameConfig.onreadystatechange = gameInitialValues;
    xmlhttpGameConfig.open("GET","http://php.tramasoli.com/sokoban.php",false);
    xmlhttpGameConfig.send();
    eraseChildren(containerElement);
    gameInitialValues();
    containerElement.appendChild(GAME_TABLE);
    window.onkeydown=function(e) {
        if (REMAINING_OBJECTIVES!==0) {
            //IMPLEMENTAÇÃO DO TURNO DO JOGO
            var evtobj=window.event? event : e;
            var keyCode=evtobj.charCode? evtobj.charCode : evtobj.keyCode;
            var oldCharPos = {
                posX: CHAR_POS.posX,
                posY: CHAR_POS.posY
            };
            var newCharPos = {
                posX: CHAR_POS.posX,
                posY: CHAR_POS.posY
            };
            var movimentoLegal=false;
            switch(keyCode) {
                case KEYS.down:
                    newCharPos.posY++;
                    break;
                case KEYS.up:
                    newCharPos.posY--;
                    break;
                case KEYS.right:
                    newCharPos.posX++;
                    break;
                case KEYS.left:
                    newCharPos.posX--;
                    break;
                default:
                    //Não contabiliza "teclas mortas"
                    MOVIMENTOS--;
            }
            if (newCharPos.posX<0)
                newCharPos.posX=0;
            else if (newCharPos.posX>=TABULEIRO_SIZE.x)
                newCharPos.posX=TABULEIRO_SIZE.x-1;
            if (newCharPos.posY<0)
                newCharPos.posY=0;
            else if (newCharPos.posY>=TABULEIRO_SIZE.y)
                newCharPos.posY=TABULEIRO_SIZE.y-1;
            switch(GAME_TABLE.rows[newCharPos.posY].cells[newCharPos.posX].className) {
                case EnumObjectsTypes.EMPTY:
                    movimentoLegal=true;
                    CHAR_POS = newCharPos;
                    break;
                case EnumObjectsTypes.BOX:
                case EnumObjectsTypes.BOX_OBJECTIVE:
                    diffX=newCharPos.posX-oldCharPos.posX;
                    diffY=newCharPos.posY-oldCharPos.posY;
                    try {
                        movimentoLegal=( GAME_TABLE.rows[oldCharPos.posY+(2*diffY)].cells[oldCharPos.posX+(2*diffX)].className===EnumObjectsTypes.EMPTY ) ||( GAME_TABLE.rows[oldCharPos.posY+(2*diffY)].cells[oldCharPos.posX+(2*diffX)].className===EnumObjectsTypes.OBJECTIVE );
                        if (movimentoLegal) {
                            if ( GAME_TABLE.rows[newCharPos.posY].cells[newCharPos.posX].className === EnumObjectsTypes.BOX_OBJECTIVE) {
                                REMAINING_OBJECTIVES++;
                            }
                            if(GAME_TABLE.rows[newCharPos.posY+(diffY)].cells[newCharPos.posX+(diffX)].className===EnumObjectsTypes.OBJECTIVE) {
                                GAME_TABLE.rows[newCharPos.posY+(diffY)].cells[newCharPos.posX+(diffX)].className=EnumObjectsTypes.BOX_OBJECTIVE;
                                REMAINING_OBJECTIVES--;
                            }
                            else {
                                GAME_TABLE.rows[newCharPos.posY+(diffY)].cells[newCharPos.posX+(diffX)].className=EnumObjectsTypes.BOX;
                            }
                            CHAR_POS = newCharPos;
                        }
                    } catch(ex) {
                        movimentoLegal=false;
                    }
                    break;
                case EnumObjectsTypes.OBJECTIVE:
                    movimentoLegal=true;
                    CHAR_POS = newCharPos;
                    break;
            }
            GAME_TABLE.rows[oldCharPos.posY].cells[oldCharPos.posX].className=(GAME_TABLE.rows[oldCharPos.posY].cells[oldCharPos.posX].className===EnumObjectsTypes.CHARACTER_OBJECTIVE)?EnumObjectsTypes.OBJECTIVE:EnumObjectsTypes.EMPTY;
            GAME_TABLE.rows[CHAR_POS.posY].cells[CHAR_POS.posX].className=(GAME_TABLE.rows[CHAR_POS.posY].cells[CHAR_POS.posX].className===EnumObjectsTypes.OBJECTIVE || GAME_TABLE.rows[CHAR_POS.posY].cells[CHAR_POS.posX].className===EnumObjectsTypes.BOX_OBJECTIVE)?EnumObjectsTypes.CHARACTER_OBJECTIVE:EnumObjectsTypes.CHARACTER;
            if(REMAINING_OBJECTIVES===0) {
                alert("Parabéns! Você venceu!");
                document.getElementById("game-status").innerHTML="[STATUS] Jogo terminado.";
            }
            else {
                document.getElementById("game-status").innerHTML="[STATUS] Posição X: "+CHAR_POS.posX+"; Posição Y: "+CHAR_POS.posY+"; Movimentos: "+(++MOVIMENTOS)+"; Movimento "+(movimentoLegal?"legal":"ilegal")+"; Objetivos restantes: "+REMAINING_OBJECTIVES;
            }
        }
    };
}


// FUNÇÕES AUXILIARES
function eraseChildren(element) {
     while (element.firstChild)
        element.removeChild(element.firstChild);
}

function isEmpty(value) {
    return (value===null || value===undefined || value==="" || value===0);
}


//Ok, 98% autoral.
// source: http://stackoverflow.com/questions/596481/simulate-javascript-key-events
function simulateKeyEvent(keyCode) {
    var keyboardEvent = document.createEvent("KeyboardEvent");
    var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";


    keyboardEvent[initMethod](
                       "keydown", // event type : keydown, keyup, keypress
                        true, // bubbles
                        true, // cancelable
                        window, // viewArg: should be window
                        false, // ctrlKeyArg
                        false, // altKeyArg
                        false, // shiftKeyArg
                        false, // metaKeyArg
                        keyCode, // keyCodeArg : unsigned long the virtual key code, else 0
                        0 // charCodeArgs : unsigned long the Unicode character associated with the depressed key, else 0
    );
    document.dispatchEvent(keyboardEvent);
}